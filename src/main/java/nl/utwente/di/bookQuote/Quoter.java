package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    Map<String, Double> bookPrice = new HashMap<>();

    public Quoter() {
        bookPrice.put("1", 10.0);
        bookPrice.put("2", 45.0);
        bookPrice.put("3", 20.0);
        bookPrice.put("4", 35.0);
        bookPrice.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        if (bookPrice.containsKey(isbn)) {
            return bookPrice.get(isbn);
        } else {
            return 0;
        }
    }
}
